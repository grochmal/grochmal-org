
about:config

right-click on any link and select "New->String"
string name: network.protocol-handler.app.magnet
string value: /usr/bin/azureus

right-click on any link and select "New->Boolean"
enter name: network.protocol-handler.handler.external.magnet
enter value: yes

Restart firefox and you're ready to go.
