
Once you get into about:config, type browser.cache into the filter bar at the
top. Find browser.cache.disk.enable and set it to false by double clicking on
it. You'll then want to set browser.cache.memory.enable to true (mine seemed to
already be set as such), and create a new preference by right clicking
anywhere, hitting New, and choosing Integer. Call the preference
browser.cache.memory.capacity and hit OK. In the next window, type in the
number of kilobytes you want to assign to the cache (for example, typing 100000
would create a cache of 100,000 kilobytes or 100 megabytes). A value of -1 will
tell Firefox to dynamically determine the cache size depending on how much RAM
you have.
