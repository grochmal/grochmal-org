grochmal-org
============

Data files, configuration and scripts of my personal website located at
<http://grochmal.org>.  All code is licenced under GPL version 3 (or, at your
option, any later version for the GPL licence), and all article text is
licenced under the Creative Commons Attribution-ShareAlike 4.0 International.

The GNU GPL v3 licence can be found at:
<https://www.gnu.org/licenses/gpl-3.0.html>

The Creative Commons Attribution-ShareAlike 4.0 International licence can be
found at: <https://creativecommons.org/licenses/by-sa/4.0/>

Copying
-------

Copyright (C) 2015 Michal Grochmal

This file is part of the grochmal-org package.

grochmal-org is free software; you can redistribute and/or modify all or part
of the code under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or (at your
option) any later version.

grochmal-org is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

The COPYING file in the root directory of the project contains a copy of the
GNU General Public License.  If you cannot find this file, see
<http://www.gnu.org/licenses/>.

TODO
----

* `load-article.py` to load articles from CLI.

* clean up `uwsgi_params` for `nginx`.

* Add an article about server configuration for this website, this will finally
  publish my 3000+ lines of notes about nginx/django/uwisgi config.

