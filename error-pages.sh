#!/bin/sh

# It is better to serve error pages directly from the webserver as they can
# still be served if django crashes.  On the other hand, django's tempates
# allow for much risher error pages.  This script takes template backed error
# pages and "print" them into raw html that can then be served by the
# webserver.

EDIR=error

# ensure that the directory is there
if ! test -d "$EDIR"
then
  mkdir "$EDIR"
fi

# ensure that SITE_ID is set to 1 (must use GNU sed, because of -i)
sed -e 's/^\s*#*\s*SITE_ID\s*\=\s*[0-9]\+/SITE_ID = 1/' -i conf/settings.py
# run django's inbuilt server in the background
python manage.py runserver &
# it takes some time to start it, wait
sleep 6
# use curl to get the pages for site 1 (grochmal.org)
curl http://localhost:8000/400/ > "$EDIR/gr400.html"
curl http://localhost:8000/403/ > "$EDIR/gr403.html"
curl http://localhost:8000/404/ > "$EDIR/gr404.html"
curl http://localhost:8000/500/ > "$EDIR/gr500.html"

# change SITE_ID to 2
sed -e 's/^\s*#*\s*SITE_ID\s*\=\s*[0-9]\+/SITE_ID = 2/' -i conf/settings.py
# configuration changed, kill server and restart
kill $!
python manage.py runserver &
sleep 6
# get pages for site 2 (neptunepenguin.net)
curl http://localhost:8000/400/ > "$EDIR/np400.html"
curl http://localhost:8000/403/ > "$EDIR/np403.html"
curl http://localhost:8000/404/ > "$EDIR/np404.html"
curl http://localhost:8000/500/ > "$EDIR/np500.html"
# SITE_ID back to 1, just in case
sed -e 's/^\s*#*\s*SITE_ID\s*\=\s*[0-9]\+/SITE_ID = 1/' -i conf/settings.py
# no need for the django server anymore, kill it again
kill $!

# create the generic error pages
sed -e s/400/4xx/ "$EDIR/gr400.html" > "$EDIR/gr4xx.html"
sed -e s/400/4xx/ "$EDIR/np400.html" > "$EDIR/np4xx.html"
sed -e s/500/5xx/ "$EDIR/gr500.html" > "$EDIR/gr5xx.html"
sed -e s/500/5xx/ "$EDIR/np500.html" > "$EDIR/np5xx.html"

