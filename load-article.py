#!/usr/bin/env python
import sys, os, django, yaml, pytz, datetime

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(BASE_DIR)
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'conf.settings')
django.setup()  # django 1.7 and later

from django.conf import settings
from sagittarius.models import Section, ArticleEntry, Tag
from sagittarius.models import hash_validator, upload_alt
from sagittarius.util import md2html, make_content
from django.contrib.sites.models import Site
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.core.validators import validate_slug

# These two colud be done much better as a single generic funtion
def tag_objects(taglist):
    objs = []
    for tag in taglist:
        try: objs.append(Tag.objects.get(slug=tag))
        except ObjectDoesNotExist: pass
    if 0 == len(objs):
        print('WARNING!  No tags were added.')
    return objs
def site_objects(sitelist):
    objs = []
    for site in sitelist:
        try: objs.append(Site.objects.get(domain=site))
        except ObjectDoesNotExist: pass
    if 0 == len(objs):
        print('WARNING!  No sites were added, the page cannot be viewed.')
    return objs

# And these two as well :(
def get_user(user):
    try:
        return User.objects.get(username=user)
    except ObjectDoesNotExist:
        print('Cannot find user "%s" in the database.  ABORTING!' % user)
    return None
def get_section(section):
    try:
        return Section.objects.get(slug=section)
    except ObjectDoesNotExist:
        print('Cannot find section "%s" in the database.  ABORTING!' % section)
    return None

### example entry: asm-syscalls.yaml ####
#priority   : 20
#featured   : true
#menu_entry : asm syscalls
#hash       : 4612ec
#author     : bluepenguin
#section    : hacks
#site       : [ grochmal.org , neptunepenguin.net ]
#tags       : [ p2p , exploit ]
#title      : Linux Systemcalls in Assembler
#pub_date   : 2015-05-12T13:32:17.12+00
#mod_date   : 2015-05-12T13:32:17.12Z
#status     : 1
#excerpt    : |
#  We will discuss how to make system calls to the Linux kernel directly
#  from machine code. We will use assembler to produce the machine code,
#  although writing the machine code itself from it is just a matter of
#  translation. System calls in assembler are just nameless integers, and we
#  will as well understand how to find which number correspond to which
#  system call.

TZ = pytz.timezone(settings.TIME_ZONE)
MAND_STR = [ 'menu_entry' , 'title' , 'excerpt' ]  # slug is the filename
MAND_REF = { 'author':get_user   , 'section':get_section }
OPT_LIST = { 'site':site_objects , 'tags':tag_objects    }
OPT_STR  = { 'priority':0        , 'featured':False
           , 'status':2          , 'hash':'313373'       }
OPT_DATE = { 'pub_date' : TZ.localize(datetime.datetime.now())
           , 'mod_date' : TZ.localize(datetime.datetime.now())
           }
VAL_LIST = { 'hash' : hash_validator }

has_key_type = lambda o,d,k: k in d and type(d[k]) == type(o[k])
has_key_list = lambda d,k:   k in d and type(d[k]) == list
has_key_date = lambda d,k:   k in d and type(d[k]) == datetime.datetime
# type(yaml.load('2015-05-12T13:32:17.12+00')) == datetime.datetime
# type(yaml.load('2015-05-12T13:32:17.12Z')) == datetime.datetime

def validate_meta(params):
    for k in VAL_LIST:
        try:
            VAL_LIST[k](params[k])
        except ValidationError as e:
            print(e)
            print('Bad value for %s, giving up' % k)
            return None
    return params

def check_meta(info, slug):
    params = {'slug':slug}
    for m in MAND_STR + list(MAND_REF.keys()):
        if not m in info:
            print('%s key is missing, giving up' % m)
            return None
    for ms in MAND_STR:
        if not str == type(info[ms]):
            print('%s key must be a string, giving up' % ms)
            return None
        params[ms] = info[ms]
    for mr in MAND_REF:
        params[mr] = MAND_REF[mr](info[mr]) if mr in info else None
        if not params[mr]: return None
    for ol in OPT_LIST:
        params[ol] = OPT_LIST[ol](info[ol]) if has_key_list(info,ol) else []
    for os in OPT_STR:
        params[os] = info[os] if has_key_type(OPT_STR,info,os) else OPT_STR[os]
    for od in OPT_DATE:
        params[od] = info[od] if has_key_date(info,od) else OPT_DATE[od]
    return validate_meta(params)

def yaml_failure(metafile):
    try:
        metainfo = yaml.load(open(metafile, 'r'))
    except yaml.YAMLError as e:
        print('%s: not a YAML file' % metafile)
        if hasattr(e, 'problem_mark'):
            m = e.problem_mark
            print("%s: parse error at (%s:%s)"
                    % (metafile, m.line+1, m.column+1))
        return None
    if not dict == type(metainfo):
        print('%s: must be a dictionary' % metafile)
        return None
    return metainfo

def add_media(article, obj):
    from django.core.files import File
    metainfo = yaml_failure(article['media_meta'])
    if not metainfo: return None
    print("Hanven't implemented media processing yet.  Sorry, no media.")
    return None
    # http://stackoverflow.com/questions/15332086/saving-image-file-through-django-shell
    m = MediaEntry.objects.update_or_create(article=obj, mname=params['mname'])
    loc = os.path.join(article['media'], params['fname'])
    # use upload_alt to get the same result as admin
    fname = upload_alt(m, params['fname'])
    m.fname.save(fname, File(open(loc, 'r')))
    m.save()

def process_article(article):
    try:
        validate_slug(article['slug'])
    except ValidationError:
        print('%s: bad slug, giving up' % article['slug'])
        return None

    metainfo = yaml_failure(article['meta'])
    if not metainfo: return None
    params = check_meta(metainfo, article['slug'])
    if not params: return None

    params['body']         = open(article['body']).read()
    params['body_html']    = md2html(params['body'])
    params['excerpt_html'] = md2html(params['excerpt'])
    params['content_html'] = make_content(params['body'])

    badsave = [ 'section' , 'hash' , 'slug' , 'tags' , 'site' ]
    defs = { k: params[k] for k in params if k not in badsave }
    print('Saving to <domain(s)>/%s/%s/%s/'
            % ( params['section'], params['hash'], params['slug']))
    obj, cr = ArticleEntry.objects.update_or_create( section=params['section']
                                                   , slug=params['slug']
                                                   , hash=params['hash']
                                                   , defaults=defs
                                                   )
    obj.tags = params['tags']
    obj.site = params['site']
    obj.save()
    if 'media' in article:
        add_media(article, obj)

if '__main__' == __name__:
    articles = []
    for arg in sys.argv[1:]:
        stat = {}
        print('Checking', arg)
        if os.access(arg, os.R_OK):
            stat['body'] = arg
            stat['slug'] = os.path.splitext(os.path.basename(arg))[0]
        else:
            print('%s: no such file, giving up on this article' % arg)
            continue
        path, ext = os.path.splitext(arg)
        meta = path + '.yaml'
        if os.access(meta, os.R_OK):
            stat['meta'] = meta
        else:
            print('%s: no meta file, giving up on this article' % meta)
            continue
        media = path + '-media'
        if os.access(media, os.R_OK | os.X_OK) and os.path.isdir(media):
            print('%s: media dir present' % media)
            media_meta + os.path.join(media, 'meta.yaml')
            if os.access(media_meta, os.R_OK):
                stat['media'] = media
                stat['media_meta'] = media_meta
            else:
                print('%s: no such file, will not load media' % media_meta)
        articles.append(stat)
    for a in articles: process_article(a)

