"""conf URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings

from sagittarius import urls as sagittarius_urls
from sagittarius.views import Error400View , Error403View \
                            , Error404View , Error500View

handler400 = Error400View.as_view()
handler403 = Error403View.as_view()
handler404 = Error404View.as_view()
handler500 = Error500View.as_view()

urlpatterns = \
[ url(r'^data/', include(admin.site.urls))
, url(r'', include(sagittarius_urls))
]

if settings.DEBUG:
    urlpatterns += [ url(r'^400/$', handler400)
                   , url(r'^403/$', handler403)
                   , url(r'^404/$', handler404)
                   , url(r'^500/$', handler500)
                   ]

