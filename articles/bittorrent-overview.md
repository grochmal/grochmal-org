# BitTorrent Overview

*BitTorrent* is the most popular peer to peer network, it is well established
because of a very extensible protocol.  Currently the protocol is extended in
different ways by different clients and still all clients can interoperate.

## Exploit Possibility { #bt-exploit }

As BitTorrent clients implement *different specification extensions* in
different ways and need to support communicating with clients that use even
different extensions, several negotiation methods and stream parsers are
present in the clients.  The big number of parsers means that there is a high
possibility of bugs in code that process user supplied data.  Depending of the
severity of the bug, it is likely that we can exploit it to crash the client
(denial of service) or even to gain control of the execution flow of the
running process.

In control of the execution process we will *attack the user's computer*, the
computer running the BitTorrent client, with a connect back shellcode.  This
will give us access to the user's computer at the same privilege level that the
BitTorrent client is running under.  We can be confident that the connect back
shellcode will be capable of connecting as it is certain that a computer
running a BitTorrent client is connected to the internet.

As a fall-back, if we fail to find exploitable bugs in the clients, we can
explore the architecture of *BitTorrent trackers*.  Different BitTorrent
trackers are deployed in very different ways, e.g. [opentracker][] is an open
source implementation built in C and covering all protocols by itself (even the
HTTP protocol is implemented as part of that tracker), whilst [peertracker][]
is a tracker implementation in PHP that only covers the higher protocols and
expects a webserver (apache, nginx) to do the rest of the job.

[opentracker]: http://erdgeist.org/arts/software/opentracker/
[peertracker]: https://github.com/JonnyJD/peertracker

Just as BitTorrent clients, BitTorrent trackers need to deal with *protocol
extensions* and with connecting clients that might have or have not certain
extensions.  This again means a necessity for protocol negotiation, and a high
probability of bugs while processing user supplied data.

## BitTorrent Popularity { #bt-popularity }

BitTorrent is responsible for [more than 40% of internet traffic][istudy]
therefore we can be confident that many people use BitTorrent clients.  If
serious security risks can be found in BitTorrent clients these will impact a
big number of users.

[istudy]: https://www.christopher-parsons.com/Main/wp-content/uploads/2009/04/ipoque-internet-study-08-09.pdf

## BitTorrent Client Market Share { #br-market-share }

Popularity of different BitTorrent clients is skewed, and for the biggest
impact we shall look at the most used clients.  On the other hand auditing
BitTorrent clients is much easier if the source code is available, yet not all
clients make their source code available.  The following table shows the
*market share* and whether the *source code* is available for the most common
BitTorrent clients:

| client              | market share | source code available? |
| ------------------- | ------------ | ---------------------- |
| uTorrent            |        49.3% | no                     |
| Vuze (Azureus)      |        19.2% | yes                    |
| BitTorrent mainline |        10.0% | yes (not maintained)   |
| BitComet            |         4.2% | no                     |
| Transmission        |         3.7% | yes                    |
| libtorrent/rtorrent |         0.5% | yes                    |
| Other/Unknown       |         4.7% | N/A                    |

Data sources: [tribler][] and [torrentfreak][].

[torrentfreak]: https://torrentfreak.com/utorrent-keeps-bittorrent-lead-bitcomet-fades-away-110916/
[tribler]: http://forum.tribler.org/viewtopic.php?f=2&t=368

## BitTorrent Architecture { #bt-architecture }

A metadata file (.torrent) describes what is being distributed

The *torrent file* contains:

* hashes of the data, divided into small chunks
* names and size of the files
* a list of trackers

A *tracker* is a server that coordinates the distribution of the files.  The
files are distributed by *peers* (BitTorrent client users) that connect
directly to other *peers*, as directed by the *tracker*.

Users (peers) are either *seeders* or *leechers*.  *Seeders* are peers that
already have the files and only upload them to other peers.  *Leechers* are
peers that are downloading the files.  *Leechers* download chunks of files from
*seeders* and from other *leechers* whilst upload the chunks they already
downloaded to *leechers* that do not yet have these chunks.

The chunks of files transfered are called *pieces* and *blocks* in BitTorrent.
A *block* is the smallest chunk of data that is exchanged between peers when
one uploads and the other downloads.  Groups of *blocks* are called *pieces*,
this grouping into *pieces* is used in the negotiation between BitTorrent
clients for transfer.  A peer announces that he has or has not *pieces*, not
*blocks*.  If a piece is composed of 30 blocks, one peer has 29 blocks from
that piece and another has 0 blocks from that piece, both peers shall announce
that they do not have that *piece*.

## How the Process is Started { #bt-start }

One *peer* decides that he wants to share a group of files with others.  This
peer creates a *torrent file* that includes the names of the files, the
*pieces* of data and at least one tracker.  The *peer* publishes the *torrent
file* in any way he seems fit, for simplicity let's say that he publishes it on
his personal website.

The *peer* loads his *torrent file* into a *BitTorrent client*.  The
*BitTorrent client* identifies that the *peer* has the files specified in the
*torrent file* and informs the *trackers* (the ones specified in the *torrent
file*) of this fact.  The *peer* becomes the first *seeder* of this *torrent*.

A visitor to the original peer's website downloads the *torrent file* and loads
it into hers *BitTorrent client*.  The client figures out that she does not
have the files and asks the *tracker* for a *peer* to downloads the files from.
She becomes the first *leecher*.  The *tracker* informs the new *leecher* of
the *seeder*, and the *leecher* connects directly to the *seeder* to negotiate
the *pieces* to download.  This negotiation happens several times before all
*pieces* are downloaded.

Whilst the first *leecher* downloads the files from the first *seeder* a new
visitor might visit the website, download the *torrent file* and load it into
his *BitTorrent client*.  He will then be directed by the *tracker* to download
pieces of the files from both the *seeder* and the first *leecher*.

Once a *leecher* finishes the download it becomes a *seeder* and keeps
uploading file *pieces* to the network.  When more *peers* join the network the
different *pieces* containing the chunks of the files can be downloaded from
different places at the same time.  The dynamic process of directing *peers* to
download from each other is called the *swarm* in peer-to-peer networks.  In
BitTorrent the name *swarm* also refers to the group of all seeders and all
leechers currently downloading and/or uploading pieces of files for a single
torrent.

## The Negotiation { #bt-negotiation }

When one *peer* is directed to another *peer* by the tracker they negotiate
which *piece* (or *pieces*) can be transfered.  Both peers exchange *bitfields*
of the *pieces* they have and the downloading peer requests the download of
blocks that it does not have.  The peer requesting is known as an *interested
peer*.  The *uploading peer* serves (uploads) the *blocks* of the *piece* it
was asked for based on a principle of reciprocity.

## Reciprocity { #bt-reciprocity }

The principle of *reciprocity* increases cooperation between groups of *peers*
inside the *swarm*.  Each peer connects to a handful of other peers, say 30,
and from them it selects a smaller number of peers that it calls *unchoked*.
Unchoked peers are connected peers to which the *BitTorrent clinet* will upload
if they ask for data.  Lets say that the number of *unchoked* peers is 10 in
the BitTorrent client a user runs.

The *unchocking* happens in *rounds* of a couple of seconds, say 5 seconds.
Each 5 seconds a peer will select 10 best peers (we will define *best* in a
moment) and unchoke them.  It will then upload to the unchocked peers.  It will
also inform all *unchocked* peers that they became unchocked and all other
peers that they're *chocked*.

Selecting peers to be *unchocked* is based on the highest contribution of these
peers to the selecting peer's download speed, i.e. the peers that sent us more
data.  Yet, some *unchocked* peers are selected at random.  Say, that from the
10 peers that shall be selected to be unchocked peers 7 are selected based on
the amount of data downloaded from them and other 3 peers are selected at
random.  This random selection allow a peer to *probe* *chocked* peers for
better download speeds.

The principle of *reciprocity* is *not* part of the main *BitTorrent protocol*,
and it is implementation dependent.  This generates some ambiguity on how
different clients operate.

Until now we only considered that a single *peer* is connected to a single
network.  A *single network* means that it is downloading and/or uploading
files defined in a single *torrent file*.  In reality that is hardly true, a
user running a *BitTorrent client* will have it loaded with several *torrent
files* and will be downloading and uploading several groups of files at the
same time.  The fact the BitTorrent clients are connected to several swarms at
a time results in different implementations of the *reciprocity* principle.

Ambiguous behaviour happens because some BitTorrent clients evaluate chocking
and unchocking of peers only within a *single swarm* (i.e. a single torrent
file), whilst other clients evaluate it based on *all networks* currently
connected to (or connected once upon a time) by the client.

## Piece Selection { #bt-piece-selection }

A peer in a swarm that do not yet have most of the pieces might request any of
them, yet BitTorrent specifies how the selection of pieces shall be performed.
A peer will connect to 30 other peers (according to our earlier assumption) and
exchange *bitfields* with each of them.  The peer therefore knows of the
distribution of pieces in the part of the swarm he knows about, called the
*local swarm*.  It will try to request for the pieces that are rarest in the
*local swarm*, this behaviour is called *local rarest first*.

The *local rarest first* selection method tries to prevent pieces from being
lost.  Peers do leave the swarm: after they finished the download, once the
user wants to shutdown the machine running the BitTorrent client or once a
seeder decides that he is not interested in distributing the files anymore.  If
a certain piece is located only on a peer that leaves the swarm that piece is
lost.  If that peer do not return to the swarm that piece cannot be reached and
the original files cannot be retrieved in full.  The fact that a piece is
irreversibly lost is known as *swarm failure*.  *Local rarest selection* tries
to ensure that a piece that is on a single peer will be quickly replicated to
another peer, this way preventing or postponing swarm failure.

## Peer Lists Provided by a Tracker { #bt-peer-list }

When a peer connects to a tracker it issues a request for a number IPs of other
peers.  The tracker responds the request with a *random* assortment of peer IPs
and a time after which the peer should repeat its request to the tracker.  The
tracker uses these repeated requests as life beats of the peers in the swarm,
i.e. a peer that do not repeat the request is considered dead and its IP will
not be given to other peers.

A peer keeps a list of other peers IPs and stays connected to a *subset* of
them.  If one peer from the list shuts down or disconnects another peer is
selected and connected to.  If the number of peers that can be connected to
falls below a certain level the peer will issue another request to the tracker,
even if the time for a new request has not passed yet.

## Documented Attacks { #bt-doc-attacks }

TODO

Peer-to-peer is a popular network application, and BitTorrent is a popular
application within peer-to-peer, therefore several attack have been devised
against BitTorrent networks.  Yet, the vast majority of the devised attacks see
BitTorrent as a *distributed system* and try to interfere with the
effectiveness of the swarm.

*Selfish peer* is one popular and well documented attack, also called
*free-rider* [Konrath][konrath].

*sybil attack*: the peer presents itself as several peers, it acquires several
identities from the tracker.

*lying piece possession* [Konrath][konrath]: announce that you have certain
pieces making them not rare.  Choke all peers requesting the piece (as you do
not have it).  If only the lies stay the piece can disappear from the swarm.
Yet you cannot lie that you have all pieces, it would not be possible to make a
certain piece rare and then extinct.

[konrath]: doi://10.1109/P2P.2007.14

*eclipse attack* if an attacker have a lot of computational resources he can
create several *sybil peers* (liars) and flood a swarm with them.  A sybil peer
is not limited to connection restrictions of typical clients and can connect to
many more peers.  With more sybils than spec abidng clients there is a high
probability that the abiding clients will be always choked, moreover seeders
might leave the swarm because they believe that there are enough seeders
already.

*downloading from fastest peers* or *downloading from seeders only*
[Liogkas][liogkas]

[liogkas]: http://iptps06.cs.ucsb.edu/papers/Liogkas-BitTorrent06.pdf

## Distributed Hash Table { #dht }

The fact that is it an extension

A description of it

## The work that will be done { #proto }

TODO

On the other hand we plan to build attacks on the BitTorrent clients as
applications.

Prototype client

How it is based on the spec

# References

For now all the references are in the text

