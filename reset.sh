#!/bin/sh

# full reset:
#rm conf/db.sqlite3

# light reset
python manage.py flush

python manage.py makemigrations
python manage.py migrate
#python manage.py createsuperuser  # use a fixture instead

# from conf/fixtures and sagittarius/fixtures
python manage.py loaddata users
python manage.py loaddata sites
python manage.py loaddata tags
python manage.py loaddata sections
python manage.py loaddata links
python manage.py loaddata test-articles

