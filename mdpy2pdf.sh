#!/bin/sh
#
# mdpy2pdf.sh - converts markdown extra into pdf
#
# It uses the python implementation of PHP markdown extra and HTML as the
# intermediate media.  This is to avoid several bugs in the way pandoc
# processes markdown extra files.

mkpdf () {
    echo Writing $2
    markdown_py -x extra <$1 |
        pandoc -f html -t latex -o $2
}

if test "$1x" == "x"
then
    echo 'Usage: mdpy2pdf.sh input-file [ output-file ]'
    echo
    echo '    If not provided `output-file` is `input-file | s/\.md$/.pdf/`'
elif ! test -r $1 -a -f $1
then
    echo $1: file cannot be read
elif ! which markdown_py >/dev/null 2>&1
then
    echo 'Cannot find `markdown_py`, is python markdown installed?'
elif ! which pandoc >/dev/null 2>&1
then
    echo 'Cannot find `pandoc`, is it installed?'
else
    out="$2"
    if test "${out}x" == "x"
    then
        out="${1%.md}.pdf"
    fi
    mkpdf "$1" "$out"
fi

