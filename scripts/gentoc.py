#!/usr/bin/env python
import re, os, sys

def make_toc(ls, tree, level):
    while ls:
        s,t,i = ls.pop()
        if s == level:
            tree.append({'title':t , 'id':i , 'sub':None})
        elif '#' == s:
            ls.append((s,t,i))
            sub = []
            make_toc(ls, sub, s)
            tree.append({'title':t , 'id':i , 'sub':sub})
        elif '' == s:
            ls.append((s,t,i))
            return

def print_toc(indent, inc, ls):
    html = ''
    for d in ls:
        if not d['sub'] is None:
            html += '%s<li>\n' % indent
            html += '%s  <a href="#%s">%s</a>\n' % (indent,d['id'],d['title'])
            html += '%s  <ul>\n' % indent
            html += print_toc(indent+inc, inc, d['sub'])
            html += '%s  </ul>\n' % indent
            html += '%s</li>\n' % indent
        else:
            html += ( '%s<li><a href="#%s">%s</a></li>\n'
                    % (indent,d['id'],d['title'])       )
    return html

def make_content(text):
    r = re.findall('\n\#\#(#)?\s+([^{#]+)\s+\#*\s*{\s*\#([\w-]+)\s*}', text)
    r.reverse()
    tree = []
    make_toc(r, tree, '')
    html  = '  <ol>\n'
    html += print_toc('', '    ', tree)
    html += '  </ol>'
    return html

def usage():
    print sys.argv[0], '<mardown file>'
    exit(1)

if '__main__' == __name__:
    if 1 >= len(sys.argv): usage()
    input = sys.argv[1]
    if not os.access(input, os.R_OK):
        print '%s: no such file' % input
        exit(2)
    f = open(input).read()
    print make_content(f)

